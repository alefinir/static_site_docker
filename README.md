# A Demo Static Site using Docker and Nginx

This repo contains the code for building a static site using an Nginx container. The code for the site is saved in `./content` folder, and the Nginx config is in `./conf/default.conf`. The Dockerfile contains commands to build a preconfigured Docker Image.

To build a Docker image from the Dockerfile, run the following command from inside this directory:

```sh
$ docker build -t mynginx_image .
```
This will produce the following output:

```sh
Sending build context to Docker daemon  508.4kB
Step 1/2 : FROM nginx
 ---> f09fe80eb0e7
Step 2/2 : COPY content /usr/share/nginx/html
 ---> 77d9dc08d3e3
Removing intermediate container 9cf6d8ecb0f7
Successfully built 77d9dc08d3e3
Successfully tagged mynginx_image:latest
```

To run the image in a Docker container, use the following command:
```sh
$ docker run --name mynginx -p 80:80 -d mynginx_image
```

If you visit `http://localhost` in your browser, you should be able to see the static site.